import pandas as pd


def read_in_csv(name: str):
    return pd.read_csv(name)
